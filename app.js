
const validator = require('validator')
const chalk = require('chalk')
const yargs = require('yargs')
const notes = require('./notes')

//Customize yargs version
yargs.version('1.1.0') //process things easily to add stuffs to files or objects

//Create add command
yargs.command({
    command: 'add',
    describe: 'Add a new note',
    builder: { // What we are passing as arguments
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Note Body',
            demandOption: true,
            type: 'string'
        }
    },
    handler: function (argv) { // What is going to run when the command is ran
        notes.addNote(argv.title, argv.body)
    }
})

//Create remove command
yargs.command({
    command: 'remove',
    describe: 'Remove a new note',
    builder: { // What we are passing as arguments
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        notes.removeNote(argv.title)
    }
})

//Create list command
yargs.command({
    command: 'list',
    describe: 'List your note',
    handler() {
        notes.listNotes()
    }
})

//Create read command
yargs.command({
    command: 'read',
    describe: 'Read a note',
    builder: { // What we are passing as arguments
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        notes.readNote(argv.title)
    }
})

yargs.parse()

//add, remove, read, list, update
// console.log(yargs.argv)

// if (command === 'add') {
//     console.log('Adding Note')
// } else if (command === 'remove') {
//     console.log('Removing Note')
// }





// const add = require('./utils')
// const sum = add(4, -2)
// console.log(sum)

// const fs = require('fs')
// fs.writeFileSync("notes.txt", ' My name is Douglas Kamseu.')
// fs.appendFileSync('notes.txt', ' I am 24 years old')