const chalk = require('chalk')
const fs = require('fs')

//Add a new note 
const addNote = (title, body) => {
    const notes = loadNotes()
    // const duplicateNotes = notes.filter((note) => note.title === title)
    const duplicateNote = notes.find((note) => note.title === title )


    if (!duplicateNote) {
        notes.push({
            title: title,
            body: body
        })

        saveNotes(notes)

        console.log('New note added !')
    }
    else {
        console.log(chalk.red.inverse('Note title taken! '))
    }
}

//Save a note in a Json File as a JSON string
const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes) //convert object to json string
    fs.writeFileSync('notes.json', dataJSON)
}

//Reading a JSON file and converting JSON string to an object
const loadNotes = () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    }
    catch (e) {
        return []
    }
}


//Remove notes
const removeNote = (title) => {
    const notes = loadNotes()
    //Removing notes where notes title is not equal to title
    const notesToKeep = notes.filter((note) => note.title !== title);

    //Check if the notes array and notes to keep array have different length
    if(notes.length > notesToKeep.length) {
        console.log(chalk.green.inverse('Note removed'))
        saveNotes(notesToKeep)
    }
    else {
        console.log(chalk.red.inverse('No Note found'))
    }
    
}

const listNotes = () => {
    const notes = loadNotes()

    console.log(chalk.inverse('Your notes'))

    notes.forEach(note => console.log(note.title));
}

const readNote = (title) => {
    const notes = loadNotes()

    const note = notes.find((note) => note.title === title )

    if (note){
        console.log(chalk.inverse(note.title))
        console.log(note.body);
    }
    else {
        console.log(chalk.red.inverse('Note not found!'))
    }

}

module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}